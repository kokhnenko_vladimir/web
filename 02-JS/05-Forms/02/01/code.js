let valueForGender = "";
let valueForSpec = "";
idRegistration.onclick = function () {
    if (idLogin.value.length == 0) {
        alert("Заполните имя, пожалуйста.");
        return false;
    }
    if (idPassword1.value.length < 3 || idPassword1.value.length > 10) {
        alert("Длина пароля должна быть от 3 до 10 символов.");
        return false;
    }
    if (idPassword2.value !== idPassword1.value) {
        alert("Пароль и подтверждение не совпадают.");
        return false;
    }
    if (idFullName.value.length == 0) {
        alert("Заполните полное имя, пожалуйста.");
        return false;
    }

    let genderElements = document.getElementsByName('gender');
    let isTheGenderSelected = true;

    for (let gender of genderElements) {
        if (gender.checked) {
            valueForGender = gender.value;
            isTheGenderSelected = false;
            break;
        }
    }

    if (isTheGenderSelected) {
        alert("Укажите пол, пожалуйста");
        return false;
    }

    let specElements = document.getElementsByName('spec');
    let isTheSpecSelected = true;

    for (let spec of specElements) {
        if (spec.checked) {
            valueForSpec = spec.value;
            isTheSpecSelected = false;
            break;
        }
    }

    if (isTheSpecSelected) {
        alert("Укажите специализацию, пожалуйста");
        return false;
    }

    if (idPost.options.selectedIndex == 0) {
        alert("Укажите должность, пожалуйста");
        return false;
    }

    let resultString = `file:///F:/Repositories/Web/02-JS/05-Forms/02/01/resultIndex.html?idLogin=${idLogin.value}&fullName=${idFullName.value}&gender=${valueForGender}&valueForSpec=${valueForSpec}&idPost=${idPost.options[idPost.options.selectedIndex].value}`
    let newWindow = window.open(resultString, 'Result Page');
}

