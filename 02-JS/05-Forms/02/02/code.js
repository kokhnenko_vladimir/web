let timer = 10;
let resultTest = 0;

let question1 = {
    header: "Вопрос 1 из 12",
    questionName: "Для чего можно использовать JavaScript?",
    answerOptions: [
        "Для создания самостоятельно исполняемой программы",
        "Для обработки событий на web-странице",
        "Для добавления динамики и 'интелекта' в web-страницу",
        "все варианты верны"
    ],
    trueAnswers: [
        1, 2
    ]
};

const timeForAnswer = 10;

let questions = [
    question1
];

window.onload = function () {
    for (let question of questions) {

        idQuestion.textContent = `${question.questionName}`
        idHeader.innerHTML = `<b>${question.header}</b>`;

        for (let i = idAnswers.children.length - 1; i >= 0; i--) {
            idAnswers.children[i].remove();
        }

        let k = 0;
        for (let answer of question.answerOptions) {
            let tr = document.createElement('tr');
            let td = document.createElement('td');
            let input = document.createElement('input');
            input.type = "checkbox";
            input.name = "answerOptions";
            input.value = k++;
            td.append(input);
            td.append(` ${answer}`);
            tr.append(td);
            idAnswers.append(tr);
            console.dir(td);
        }
    }
}

let timerId = setInterval(() => {
    idRemainder.innerText = --timer;
    if (timer == 0) {
        clearInterval(timerId);
        setTimeout(() => alert(`Время вышло, ваш результат: ${resultTest}`), 100);
    }
}, 500);

idSubmit.onclick = function () {
    debugger;
    clearInterval(timerId);
    let answersUser = document.getElementsByName("answerOptions");

    let countTrueAnswers = 0;
    for (let i = 0; i < answersUser.length; i++) {
        if (answersUser[i].checked){
            for (let j = 0; j < question1.trueAnswers.length; j++) {
                if (answersUser[i].value == question1.trueAnswers[j]) {
                    countTrueAnswers++;
                }
            }
        }
    }
    if(countTrueAnswers == question1.trueAnswers.length) {
        ++resultTest;
        alert(`Поздравляем, тест сдан на бал ${resultTest}`);
    }
}