const towns = [
    "Киев", "Харьков", "Одесса", "Днепр", "Запорожье", "Львов"
];

function generateLines() {
    for (let town1 of towns) {
        for (let town2 of towns) {
            if (town1 != town2) {
                let option = document.createElement('option');
                option.text = `${town1}-${town2}`;
                DestinationList.prepend(option);
            }
        }
    }
}

window.onload = generateLines();

Search.onclick = function () {

    if (DateSend.value == "") {
        return;
    }

    for (let i = Seats.childNodes.length - 1; i >= 0; i--) {
        Seats.childNodes[i].remove();
    }

    let armor = [];
    let totalArmor = randomInteger(0, 27);
    for (let i = 0; i < totalArmor; i++) {
        armor[i] = randomInteger(0, 1);
    }

    let tr1 = document.createElement('tr');
    let tr2 = document.createElement('tr');

    for (let i = 0; i < 28; i++) {
        let td = document.createElement('td');
        let input = document.createElement('input');
        input.type = "checkbox";
        input.value = i + 1;

        if (i < totalArmor && armor[i] != 0) {
            input.checked = armor[i];
            input.disabled = true;
        }

        let span = document.createElement('span');
        span.textContent = i + 1;

        td.prepend(input);
        td.prepend(span);
        if (i % 2 != 0) {
            tr1.append(td);
        }
        else {
            tr2.append(td);
        }
    }

    Seats.prepend(tr1);
    Seats.prepend(tr2);
}

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

Seats.onclick = function (event) {
    if (event.target.checked == true) {
        addTicket(event);
    }
    else {
        deleteTicket(event);
    }

    Result.children.length > 0 ? Submit.disabled = false : Submit.disabled = true;
}

function addTicket(event) {
    let tr = document.createElement('tr');
    let td1 = document.createElement('td');
    td1.textContent = DestinationList.value;
    let td2 = document.createElement('td');
    td2.textContent = DateSend.value;
    let td3 = document.createElement('td');
    td3.textContent = event.target.value;

    tr.append(td1);
    tr.append(td2);
    tr.append(td3);

    Result.append(tr);
}

function isCurrentTicket(event, tr) {
        return tr.children[0].textContent == DestinationList.value && tr.children[1].textContent == DateSend.value && tr.children[2].textContent == event.target.value;
}

function deleteTicket(event) {
    let tbody = Result.children;

    for (let tr of tbody) {
        if (isCurrentTicket(event, tr))
            tr.remove();
    }
}