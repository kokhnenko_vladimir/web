window.onload = function () {
    debugger;
    
    let href = window.location.href;
    let reg = /(\d)$/;
    let result = href.match(reg)?.[0];

    if(result) {
        localStorage.clear();
    }

    if(localStorage.length) {
        document.location.href = `/html/userInfo.html?email=${localStorage.getItem("email")}`;
    }
}

idSignUp.onclick = function() {
    let regMail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (!regMail.test(idEmail.value)) {
        alert('Введите корректный e-mail, пожалуйста');
        return false;
    }
    
    let regPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    if (!regPass.test(idPassword1.value)) {
        alert("Пароль не соответствует требованиям");
        return false;
    }

    if(idPassword1.value != idPassword2.value) {
        alert("Пароль и подтверждение не совпадают");
        return false;
    }

    localStorage.setItem("email", idEmail.value);

    return true;
}