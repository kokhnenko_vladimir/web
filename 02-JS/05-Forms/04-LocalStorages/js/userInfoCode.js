window.onload = function () {
    // let href = window.location.href;
    // let reg1 = /([\w]+%40[A-Za-z]+\.[A-Za-z]+)/;
    // let reg2 = /([\w]+@[A-Za-z]+\.[A-Za-z]+)/;
    // let regChange = /(%40)/;

    // if(reg1.test(href)) {
    //     result = href.match(reg1)[0].replace(regChange, "@");
    // }
    // else {
    //     result = href.match(reg2)[0];
    // }

    let href = window.location.href;
    let reg1 = /([\w]+(%40|@)[A-Za-z]+\.[A-Za-z]+)/;
    let regChange = /(%40)/;
    
    let result = href.match(reg1)?.[0].replace(regChange, "@");

    idShowEmail.textContent = result;
    
    if (localStorage.length) {
        idFirstName.value = localStorage.getItem("idFirstName");
        idLastName.value = localStorage.getItem("idLastName");
        idBirthday.value = localStorage.getItem("idBirthday");
        idPhone.value = localStorage.getItem("idPhone");
        idSkype.value = localStorage.getItem("idSkype");
    }
}

idSave.onclick = function () {
    let patName = /[^A-Za-zА-Яа-я]/;

    if (patName.test(idFirstName.value)) {
        alert("Имя должно состоять только из буквенных символов");
        return false;
    }

    if (!idFirstName.value.length) {
        alert("Укажите имя");
        return false;
    }

    if (patName.test(idLastName.value)) {
        alert("Фамилия должна состоять только из буквенных символов");
        return false;
    }

    if (!idLastName.value.length) {
        alert("Укажите фамилию");
        return false;
    }

    if (!idBirthday.value.length) {
        alert("Введите дату рождения");
        return false;
    }

    let patYear = /(19|20)\d{2}/;

    if (!patYear.test(idBirthday.value)) {
        alert("Укажите реальную дату рождения");
        return false;
    }

    if (!idPhone.value.length) {
        let patPhone = /\d{3}[-\s]\d{3}[-\s]\d{4}/;
        if (!patPhone.test(idPhone.value)) {
            alert("Неправильно введен номер");
            return false;
        }
    }


    debugger;
    localStorage.setItem("idFirstName", `${idFirstName.value}`);
    localStorage.setItem("idLastName", `${idLastName.value}`);
    localStorage.setItem("idBirthday", `${idBirthday.value}`);
    localStorage.setItem("idPhone", `${idPhone.value}`);

    if (idSkype.value.length) {
        localStorage.setItem("idSkype", `${idSkype.value}`);
    }
}

idExit.onclick = function() {
    localStorage.clear();
}