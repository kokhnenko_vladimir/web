let countRightAns = 0;
let currentQuestion = {
    question: {},
    indexQuestion: 0
}

let q1 = {
    name: "В аквариуме 12 рыб, и все, кроме 9, умерли. Сколько живых рыб осталось в аквариуме?",
    variantAnswers: ["3 рыбы", "7 рыб", "9 рыб"],
    correctAnswer: { index: 2, text: "9 рыб" }
};

let q2 = {
    name: "Может ли мужчина жениться на сестре своей вдовы?",
    variantAnswers: ["да", "нет"],
    correctAnswer: { index: 1, text: "нет" }
}

let q3 = {
    name: "Представьте, что перед вами 2 мамы, 2 дочери, 1 бабушка и 1 внучка. Сколько человек вы видите?",
    variantAnswers: ["3", "4", "5"],
    correctAnswer: { index: 0, text: "3" }
}

let q4 = {
    name: "У трёх маляров был брат Иван, а у Ивана братьев не было. Такое возможно?",
    variantAnswers: ["да", "нет"],
    correctAnswer: { index: 0, text: "да" }
}

let q5 = {
    name: "Под забором видны шесть пар лошадиных копыт. Сколько лошадей за забором?",
    variantAnswers: ["3", "6", "4"],
    correctAnswer: { index: 0, text: "3" }
}

let questions = [q1, q2, q3, q4, q5];

idStart.onclick = function () {
    idStart.hidden = !idStart.hidden;
    if (document.cookie.length) {
        let nameCook = getCookieValue("name");
        let resultCook = getCookieValue("result");
        alert(`${nameCook}, ваш предидущий резельтат состовляет ${resultCook}`);
    }

    idQuestion.textContent = q1.name;
    currentQuestion.question = q1;
    currentQuestion.indexQuestion++;

    let i = 0;
    for (let varAns of currentQuestion.question.variantAnswers) {
        let radio = document.createElement('input');
        radio.type = "radio";
        radio.name = "variantAnswers";
        radio.value = i++;
        let pRadio = document.createElement('p');
        pRadio.append(radio);
        pRadio.append(varAns);
        idQuestion.append(pRadio);
    }
}

idQuestion.onclick = function (event) {
    if (event.target.name == "variantAnswers" && event.target.value == currentQuestion.question.correctAnswer.index) {
        countRightAns++;
    }
    clearDiv();

    if (currentQuestion.indexQuestion != questions.length) {
        nextQuestion(currentQuestion.indexQuestion);
    }
    else {
        let name = prompt("Укажите пожалуйста свое имя");
        alert(`${name} вы набрали ${countRightAns} баллов`);

        let expire = new Date();
        expire.setHours(expire.getHours() + 1);
        document.cookie = `name=${name};expires=${expire.toUTCString()};`;
        document.cookie = `result=${countRightAns};expires=${expire.toUTCString()};`;
        idStart.hidden = !idStart.hidden;
    }
}

function nextQuestion(index) {
    currentQuestion.question = questions[index];
    idQuestion.textContent = currentQuestion.question.name;
    currentQuestion.indexQuestion++;

    let i = 0;
    for (let varAns of currentQuestion.question.variantAnswers) {
        let radio = document.createElement('input');
        radio.type = "radio";
        radio.name = "variantAnswers";
        radio.value = i++;
        let pRadio = document.createElement('p');
        pRadio.append(radio);
        pRadio.append(varAns);
        idQuestion.append(pRadio);
    }
}

function clearDiv() {
    for (let i = idQuestion.childNodes.length - 1; i >= 0; i--) {
        idQuestion.childNodes[i]?.remove();
    }
}

function getCookieValue(name) {
    let cookieValue = document.cookie;
    const r = new RegExp("\\b" + name + "\\b");
    let cookieStart = cookieValue.search(r);
    if (cookieStart == -1)
        cookieValue = null;
    else {
        cookieStart = cookieValue.indexOf("=", cookieStart) + 1;
        var cookieEnd = cookieValue.indexOf(";", cookieStart);
        if (cookieEnd == -1)
            cookieEnd = cookieValue.length;
        cookieValue = unescape(cookieValue.substring(cookieStart, cookieEnd));
    }
    return cookieValue;
}