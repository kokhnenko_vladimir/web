let imagePoints = "images/points.gif";
let table = document.createElement('table');
let h1, h2, m1, m2, s1, s2, p1, p2;

function createClock() {
    let div = document.createElement('div');
    let tr = document.createElement('tr');

    for (let i = 0; i < 8; i++) {
        let td = document.createElement('td');
        td.id = i;
        tr.append(td);
    }

    table.append(tr);
    div.append(table);
    document.body.append(div);

    h1 = document.getElementById('0');
    h2 = document.getElementById('1');
    m1 = document.getElementById('3');
    m2 = document.getElementById('4');
    s1 = document.getElementById('6');
    s2 = document.getElementById('7');
    p1 = document.getElementById('2');
    p2 = document.getElementById('5');
    setTime();
}

window.addEventListener("load", createClock);

function setTime() {
    let curDate = new Date();
    let tmp = curDate.getHours();
    h1.className = `Image${Math.floor(tmp / 10)}`;
    h2.className = `Image${Math.floor(tmp % 10)}`;
    
    tmp = curDate.getMinutes();
    m1.className = `Image${Math.floor(tmp / 10)}`;
    m2.className = `Image${Math.floor(tmp % 10)}`;
    
    tmp = curDate.getSeconds();
    s1.className = `Image${Math.floor(tmp / 10)}`;
    s2.className = `Image${Math.floor(tmp % 10)}`;
    
    p1.className = "ImagePoints";
    p2.className = "ImagePoints";
}

function hiddenPoints() {
    p1.className == "ImageNoPoints" ? p1.className = "ImagePoints" : p1.className = "ImageNoPoints";
    p2.className == "ImageNoPoints" ? p2.className = "ImagePoints" : p2.className = "ImageNoPoints";
}

setInterval(hiddenPoints, 500);
setInterval(setTime, 1000);