let corner = 0;
let timerId = null;

document.onmousemove = function (event) {
    let oldImage = document.getElementsByTagName('img')[0];
    oldImage?.remove();

    let image = document.createElement('img');
    image.id = "idImage";
    image.src = "images/spiral.png";
    image.alt = "logo";
    image.style = `transform: rotate(${corner += 10}deg); position: absolute; left: ${event.clientX - 50}px; top: ${event.clientY - 50}px;`;
    image.width = "100";
    image.height = "100";
    idDiv.prepend(image);

    if (timerId != null)
        clearTimeout(timerId);

    timerId = setInterval(() => {
        image.style = `transform: rotate(${corner += 10}deg); position: absolute; left: ${event.clientX - 50}px; top: ${event.clientY - 50}px;`;
        if (corner == 360)
            corner = 0;
    }, 100);
}