function createTable() {
    let curDate = new Date();
    
    let monthStartDate = new Date(curDate.getFullYear(), curDate.getMonth(), 1);
    let mainDiv = document.createElement('div');
    let tableCalendar = document.createElement('table');
    let theadCalendar = document.createElement('thead');
    let tbodyCalendar = document.createElement('tbody');
    let daysOfWeak = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];

    let tr1 = document.createElement('tr');

    for (let i = 0; i < 7; i++) {
        let th = document.createElement('th');
        th.innerText = daysOfWeak[i];
        tr1.append(th);
    }
    theadCalendar.append(tr1);
    tableCalendar.append(theadCalendar);

    let firstDay = giveCorrectDay(monthStartDate.getDay());
    let lastDayThisMonth = new Date(curDate.getFullYear(), curDate.getMonth() + 1, 0).getDate();
    let countDayPrevMonth = new Date(curDate.getFullYear(), curDate.getMonth(), 0).getDate() - firstDay + 1;
    let countDayThisMonth = 1;
    let countDayNextMonth = 1;

    for(let i = 0; i < 5; i++) {
        let tr2 = document.createElement('tr');
        for (let j = 0; j < 7; j++) {
            let td = document.createElement('td');
            
            if(i == 0 && j < firstDay) {
                td.innerText = countDayPrevMonth++;
                tr2.append(td);
            }
            else if(countDayThisMonth <= lastDayThisMonth) {
                if(countDayThisMonth == curDate.getDate())
                    td.className = "CurrentDayColor";
                td.innerText = countDayThisMonth++;
                tr2.append(td);
            }
            else {
                td.innerText = countDayNextMonth++;
                tr2.append(td);
            }
        }
        tbodyCalendar.append(tr2);
    }
    
    tableCalendar.append(tbodyCalendar);
    mainDiv.append(tableCalendar);
    document.body.append(mainDiv);
}

window.addEventListener("load", createTable);

function giveCorrectDay(day) {
    return  day > 0 ? day - 1 : 6;
}