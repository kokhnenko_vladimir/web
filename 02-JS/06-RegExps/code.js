idFullName.onkeyup = function () {
    let reg = /^[А-Яа-я]+ ([А-Я]\.?){2}$/;
    idTestResultFullName.textContent = reg.test(idFullName.value);

    idTestResultFullName.textContent == "true" ? idTestResultFullName.className = "result-true" 
        : idTestResultFullName.className = "result-false"; 
}

idEmail.onkeyup = function() {
    let reg = /^([A-Za-z][\w]+){3,16}\@[a-z]+(\.[A-Za-z]+)?(\.[A-Za-z]{2,3})$/;
    idTestResultEmail.textContent = reg.test(idEmail.value);
    idTestResultEmail.textContent == "true" ? idTestResultEmail.className = "result-true"
        : idTestResultEmail.className = "result-false"; 
}

idDate.onkeyup = function () {
    let reg = /^[\d]{2}(\.|\s|\\|-)[\d]{2}(\.|\s|\\|-)[\d]{4}$/;
    idTestResultDate.textContent = reg.test(idDate.value);
    idTestResultDate.textContent == "true" ? idTestResultDate.className = "result-true" 
        : idTestResultDate.className = "result-false"; 
}