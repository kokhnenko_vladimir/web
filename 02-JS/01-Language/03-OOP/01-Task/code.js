var days = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
var months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

class ExtendedDate extends Date {
    constructor(day, month, year) {
        let _date = super(`${month}/${day}/${year}`);
        this.date = _date;
    }

    displayDateText() {
        let result = document.getElementById("idResultDateText");
        result.innerHTML = `${days[this.date.getDay()]} ${months[this.date.getMonth()]} ${this.date.getFullYear()}`;
    }

    isTheFutureDate() {
        let curDate = new Date();
        if (curDate.getFullYear() > this.date.getFullYear() ||
            curDate.getFullYear() === this.getFullYear() && curDate.getMonth() > this.date.getMonth() ||
            curDate.getFullYear() === this.getFullYear() && curDate.getMonth() === this.date.getMonth() &&
            curDate.getDay() > this.date.getDay())
            return false;
        else
            return true;
    }

    isTheLeapYear() {
        if (this.date.getFullYear() % 4 !== 0 ||
            this.date.getFullYear() % 100 === 0 && this.date.getFullYear() % 400 !== 0)
            return false;
        else
            return true;
    }

    getNextDate() {
        return this.date.setDate(this.date.getDate() + 1);
    }
}

let inputDayText = document.getElementById("inputDay");
let inputMonthText = document.getElementById("inputMonth");
let inputYearText = document.getElementById("inputYear");

function dateInTextFunc() {
    let inputDate = new ExtendedDate(inputDayText.value, inputMonthText.value, inputYearText.value);
    inputDate.displayDateText();
}

function isTheFutureDateFunc() {
    let inputDate = new ExtendedDate(inputDayText.value, inputMonthText.value, inputYearText.value);
    let res = document.getElementById("idResultIsTheFutureDate");
    res.innerHTML = inputDate.isTheFutureDate();
}

function isTheLeapYear() {
    let inputDate = new ExtendedDate(inputDayText.value, inputMonthText.value, inputYearText.value);
    let res = document.getElementById("idResultItIsLeapEyar");
    inputDate.isTheLeapYear() ? res.innerHTML = "Высокосный" : res.innerHTML = "Обычный";
}

function getNextDate() {
    let inputDate = new ExtendedDate(inputDayText.value, inputMonthText.value, inputYearText.value);
    let res = document.getElementById("idResultNextDate");
    res.innerHTML = new Date(inputDate.getNextDate());
}