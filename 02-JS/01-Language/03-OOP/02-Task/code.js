class Marker {
    constructor(color) {
        this.color = color;
        this.inkQuantity = 200;
    }

    printText(string) {
        let result = document.getElementById("idResultConvertedTextTheCommonMarker");
        let outputString = "";
    
        for (let i = 0; i < string.length; i++) {
            if (string[i] !== ' ') {
                this.inkQuantity--;
            }
            outputString = `${outputString}${string[i]}`;
            if(this.inkQuantity <= 0)
            break;
        }
        let resultResidue = document.getElementById("idResultResidueForTheCommonMarker");
        resultResidue.innerHTML = `Остаток для обычного маркера - ${this.inkQuantity}`;
        result.innerHTML = `Преобразованный текст для обычного маркера - ${outputString}`;
        result.style.color = this.color;
    }

    changeColor(color) {
        this.color = color;
    }
}

class FillerMarker extends Marker{
    constructor(color){
        super(color);
    }
    printText(string) {
        let result = document.getElementById("idResultConvertedTextTheFillerMarker");
        let outputString = "";
    
        for (let i = 0; i < string.length; i++) {
            if (string[i] !== ' ') {
                this.inkQuantity--;
            }
            outputString = `${outputString}${string[i]}`;
            if(this.inkQuantity <= 0)
            break;
        }
        let resultResidue = document.getElementById("idResultResidueForTheFillerMarker");
        resultResidue.innerHTML = `Остаток для заполняемого маркера - ${this.inkQuantity}`;
        result.innerHTML = `Преобразованный текст для заполняемого маркера маркера - ${outputString}`;
        result.style.color = this.color;
    }

    tokenFiller() {
        this.inkQuantity = 200;
        let resultResidue = document.getElementById("idResultResidueForTheFillerMarker");
        resultResidue.innerHTML = `Остаток для заполняемого маркера - ${this.inkQuantity}`;
    }
}


let commonMarker = new Marker(document.getElementById("idColorCommonMarker").value);
let fillerMarker = new FillerMarker(document.getElementById("idColorFillerMarker").value);

function commonMarkerFunc() {
    commonMarker.printText(document.getElementById("commonText").value);
}

function fillerMarkerFunc() {
    fillerMarker.printText(document.getElementById("fillerText").value);
}

function tokenFillerFunc() {
    fillerMarker.tokenFiller();
}

function changeColorCommonMarker() {
    commonMarker.changeColor(document.getElementById("idColorCommonMarker").value);
    commonMarkerFunc();
}

function changeColorFillerMarker() {
    fillerMarker.changeColor(document.getElementById("idColorFillerMarker").value);
    fillerMarkerFunc();
}