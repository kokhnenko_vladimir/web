function work() {
    let ticket = document.getElementById('inputNum').value;
    let result = document.getElementById('dialog');
    let sumFirst = 0;
    let sumSecond = 0;

    for (let i = 0; i < 6; i++) {
        if (i < 3) {
            sumFirst += ticket % 10;
            ticket /= 10;
        }
        else {
            sumSecond += ticket % 10;
            ticket /= 10;
        }
    }

    if (Math.floor(sumFirst) === Math.floor(sumSecond)) {
        result.innerHTML = "Счастливый ;)";
    }
    else {
        result.innerHTML = "Брось каку :)";
    }
}
