let count = 0;
let result = document.getElementById('result');
let countResult = document.getElementById('count');

function work() {
    count = 0;
    result.innerText = "";
    countResult.innerText = "";
    let userNum = document.getElementById('inputNum').value;
    let d1 = 0;
    let d2 = 100;
    dTmp = Math.round(d2 / 2);

    if (!isNaN(userNum) && userNum !== undefined && userNum >= 0 && userNum <= 100) {
        do {
            if (confirm(`Ваше число входит в диапазон от ${d1} до ${dTmp}?`)) {
                d2 = dTmp;
                count++;

                if (d2 - d1 <= 2) {
                    chooseOneOfThree(d1, d2);
                    break;
                }
                dTmp = Math.round((d2 - d1) / 2) + d1;
            }
            else if (confirm(`Ваше число входит в диапазон от ${dTmp} до ${d2}?`)) {
                d1 = dTmp;
                count += 2;
                if (d2 - d1 <= 2) {
                    chooseOneOfThree(d1, d2);
                    break;
                }
                dTmp = Math.round((d2 - d1) / 2) + d1;
            }
            else if (isNaN(userNum) || userNum === undefined) {
                alert("Для начала необходимо ввсести число");
            }
            else {
                alert("Введенное число выходит за граници диапазона, повторите ввод");
                userNum = 0;
            }
        } while (true);
    }
}

function chooseOneOfThree(d1, d2) {
    if (confirm(`Ваше число ${d1}?`)) {
        count++;
        printResult(d1);
    }
    else if (confirm(`Ваше число ${d2}?`)) {
        count += 2;
        printResult(d2);
    }
    else if (confirm(`Ваше число ${d1 + 1}?`)) {
        count += 3;
        printResult(d1 + 1);
    }
}

function printResult(num) {
    result.innerHTML = `Загаданное вами число ${num}`;
    countResult.innerHTML = `Количество попыток ${count}`;
}