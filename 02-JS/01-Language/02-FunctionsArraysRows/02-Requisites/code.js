function work() {
    let firstname, secondname, lastname;
    let data = [
        "Введите ваше имя: ",
        "Введите ваше отчество: ",
        "Введите вашу фамилию: "
    ];

    for (let i = 0; i < data.length; i++) {
        let input = prompt(`${data[i]}`, "");
        
        if (input === "") {
            i--;
            continue;
        }

        let strChanged = input.toLowerCase();
        for (let index in strChanged) {

            if (strChanged.charCodeAt(index) < 97 && strChanged.charCodeAt(index) !== 46 ||
                strChanged.charCodeAt(index) > 122 && strChanged.charCodeAt(index) < 1072 ||
                strChanged.charCodeAt(index) > 1103) {
                i--;
                break;
            }
        }
    }
}