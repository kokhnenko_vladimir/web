const widthCanvas = 1000;
const heightCanvas = 980;
const centerX = widthCanvas / 2;
const centerY = heightCanvas / 2;
const radiusOutDial = 450;
const radiusInnerDial = radiusOutDial - 5;
const radiusNum = radiusInnerDial - 15;

let lengthSeconds = radiusNum - 10;
let lengthMinutes = radiusNum - 15;
let lengthHour = lengthMinutes / 1.5;

let context = idClockDial.getContext('2d');
context.strokeStyle = "black";

window.onload = function () {
    displayClock();
    window.setInterval(
        function () {
            displayClock();
        }, 1000);
}

function displayClock() {

    //Очистка экрана. 
    context.clearRect(0, 0, widthCanvas, heightCanvas);

    //Kонтур часов
    context.lineWidth = 5;
    context.beginPath();
    context.arc(centerX, centerY, radiusInnerDial, 0, Math.PI * 2, true);
    context.moveTo(centerX, centerY);
    context.closePath();
    context.stroke();


    //Рисочки часов
    let radiusPoint;
    context.lineWidth = 2;
    context.strokeStyle = "black";
    for (let i = 0; i < 60; i++) {
        context.beginPath();

        if (i % 5 == 0) {
            radiusPoint = 5;
        }
        else {
            radiusPoint = 2;
        }

        let xPointM = centerX + radiusNum * Math.cos(-6 * i * (Math.PI / 180) + Math.PI / 2);
        let yPointM = centerY - radiusNum * Math.sin(-6 * i * (Math.PI / 180) + Math.PI / 2);
        context.arc(xPointM, yPointM, radiusPoint, 0, Math.PI * 2, true);
        context.closePath();
        context.stroke();
    }


    context.strokeStyle = "black";
    for (let i = 1; i <= 12; i++) {
        context.beginPath();
        context.font = 'bold 40px sans-serif';
        let xText = centerX + (radiusNum - 45) * Math.cos(-30 * i * (Math.PI / 180) + Math.PI / 2);
        let yText = centerY - (radiusNum - 45) * Math.sin(-30 * i * (Math.PI / 180) + Math.PI / 2);

        if (i <= 9) {
            context.strokeText(i, xText - 10, yText + 15);
        }
        else {
            context.strokeText(i, xText - 25, yText + 15);
        }
        context.closePath();
        context.stroke();
    }


    //Рисуем стрелки

    let d = new Date();
    let tSec = 6 * d.getSeconds();
    let t_min = 6 * (d.getMinutes() + (1 / 60) * d.getSeconds());
    let t_hour = 30 * (d.getHours() + (1 / 60) * d.getMinutes());

    //Рисуем секунды
    context.beginPath();
    context.strokeStyle = "red";
    context.moveTo(centerX, centerY);
    context.lineTo(centerX + lengthSeconds * Math.cos(Math.PI / 2 - tSec * (Math.PI / 180)),
        centerY - lengthSeconds * Math.sin(Math.PI / 2 - tSec * (Math.PI / 180)));
    context.closePath();
    context.stroke();

    //Рисуем минуты
    context.beginPath();
    context.strokeStyle = "black";
    context.lineWidth = 5;
    context.moveTo(centerX, centerY);
    context.lineTo(centerX + lengthMinutes * Math.cos(Math.PI / 2 - t_min * (Math.PI / 180)),
        centerY - lengthMinutes * Math.sin(Math.PI / 2 - t_min * (Math.PI / 180)));
    context.closePath();
    context.stroke();

    //Рисуем часы
    context.beginPath();
    context.lineWidth = 10;
    context.moveTo(centerX, centerY);
    context.lineTo(centerX + lengthHour * Math.cos(Math.PI / 2 - t_hour * (Math.PI / 180)),
        centerY - lengthHour * Math.sin(Math.PI / 2 - t_hour * (Math.PI / 180)));
    context.closePath();
    context.stroke();

    //Рисуем центр часов
    context.beginPath();
    //context.strokeStyle = "";
    context.fillStyle = "white";
    context.lineWidth = 3;
    context.arc(centerX, centerY, 15, 0, 2 * Math.PI, true);
    context.stroke();
    context.fill();
    context.closePath();
}

window.setInterval(
    function () {
        displayClock();
    }, 1000);

