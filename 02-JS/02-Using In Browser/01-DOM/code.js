function addNewList() {
    let ul = document.createElement('ul');
    let li = document.createElement('li');
    li.innerText = "Новый список";
    ul.append(li);
    Tree.append(ul);
}

function addElementInTheEnd() {
    document.getElementById('Tree').onclick = function (e) {
        let inputText = document.getElementById('TextAddElementInTheEnd');
        let li = document.createElement('li');
        inputText.value != "" ? li.innerText = inputText.value : alert("Укажите новое название!");
        e.target.parentNode.append(li);
    }
}

function pasteElement() {
    document.getElementById('Tree').onclick = function (e) {
        let inputText = document.getElementById('TextPasteElement');
        let li = document.createElement('li');
        inputText.value != "" ? li.innerText = inputText.value : alert("Укажите новое название!");
        e.target.before(li);
    }
}

function changeText() {
    document.getElementById('Tree').onclick = function (e) {
        let inputText = document.getElementById('TextChangeText');
        inputText.value != "" ? e.target.innerText = inputText.value : alert("Укажите новое название!");
    }
}

function addNestedList() {
    document.getElementById('Tree').onclick = function (e) {
        let inputText = document.getElementById('TextAddNestedList');
        let ul = document.createElement('ul');
        let li = document.createElement('li');
        inputText.value != "" ? li.innerText = inputText.value : alert("Укажите новое название!");
        ul.append(li);
        if (!e.target.children.length) {
            e.target.append(ul);
        }
    }
}

function removeElement() {
    document.getElementById('Tree').onclick = function (e) {
        let parent = e.target.parentNode;

        if (e.target.hasChildNodes())
            while (e.target.firstChild)
                e.target.removeChild(e.target.firstChild);
        
        if (parent.children.length > 1){
            e.target.remove();
        }
        else{
            e.target.remove();    
            parent.remove();
        }
    }
}