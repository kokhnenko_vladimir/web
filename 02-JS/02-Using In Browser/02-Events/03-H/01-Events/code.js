const editText = "Press Ctrl+E for edit text";
const saveText = "Press Ctrl+S for save text";

const eventForDiv = function (event) {

    if (event.ctrlKey && event.code == "KeyE") {
        idP.innerText = saveText;
        idTextArea.innerText = idDivText.innerText;
        idDivText.hidden = !idDivText.hidden;
        idTextArea.hidden = !idTextArea.hidden;
        
        document.body.removeEventListener('keydown', eventForDiv);
        document.body.addEventListener('keydown', eventForTextArea);
        event.preventDefault();
    }
}

const eventForTextArea = function (event) {

    if (event.ctrlKey && event.code == "KeyS") {
        idP.innerText = editText;
        idDivText.innerText = idTextArea.value;
        idDivText.hidden = !idDivText.hidden;
        idTextArea.hidden = !idTextArea.hidden;

        document.body.removeEventListener('keydown', eventForTextArea);
        document.body.addEventListener('keydown', eventForDiv);
        event.preventDefault();
    }
}

document. body.addEventListener('keydown', eventForDiv);