function myMouseMove (event) {
    console.log(`X = ${event.clientX}`);
    console.log(`Y = ${event.clientY}`);

    let myDiv = window.getComputedStyle(document.getElementById('idDiv'), null);
    let borderWidth = parseInt(myDiv.borderWidth) * 2;
    let padding = parseInt(myDiv.paddingLeft) + parseInt(myDiv.paddingRight);
    let widthDiv = parseInt(myDiv.width);
    let heightDiv = parseInt(myDiv.height);
    let resultX = borderWidth + padding + widthDiv;
    let resultY = borderWidth + padding + heightDiv;
    document.getElementById('idDiv').style.width = `${resultX}px`;
    document.getElementById('idDiv').style.height = `${resultY}px`;
}

document.getElementById('idCorner').onmousedown = function (event) {
    document.getElementById('idCorner').addEventListener("mousemove", myMouseMove);
}

document.getElementById('idCorner').onmouseup = function (event) {
    document.getElementById('idCorner').removeEventListener("mousemove", myMouseMove);
}