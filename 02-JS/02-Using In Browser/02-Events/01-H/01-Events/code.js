document.getElementById('idMainDiv').onclick = function (event) {
    for (let child of event.target.children) {
        child.style.display == "none" ? child.style.display = "block" : child.style.display = "none";
    }
}

document.getElementById('idMainDiv').onmouseover = function(event) {
    event.target.className = 'Bold';
}

document.getElementById('idMainDiv').onmouseout = function(event) {
    event.target.className = 'Normal';
}