let startSelected = { y: 0, select: null };
let finishSelected = { y: 0, select: null };

document.getElementById("idMainDiv").onclick = function (event) {

    if (event.shiftKey) {
        clearNodes();
        if (startSelected.y < finishSelected.y) {
            let nextLi = startSelected.select;
            do {
                nextLi.className = "Selected";
                nextLi = nextLi.nextSibling;
            } while (finishSelected.select != nextLi);
            nextLi.className = "Selected";
        }
        else {
            let nextLi = startSelected.select;
            do {
                nextLi.className = "Selected";
                nextLi = nextLi.previousSibling;
            } while (finishSelected.select != nextLi);
            nextLi.className = "Selected";
        }
    }
    else if (event.ctrlKey) {
        event.target.className == "Normal" ? event.target.className = "Selected" : event.target.className = "Normal";
    }
    else {
        clearNodes();
        event.target.className == "Normal" ? event.target.className = "Selected" : event.target.className = "Normal";
    }
}

document.getElementById("idMainDiv").onmousedown = function (event) {
    if (event.shiftKey) {
        startSelected.y = event.clientY;
        startSelected.select = event.target;
        event.preventDefault();
    }
}

document.getElementById("idMainDiv").onmouseup = function (event) {
    if (event.shiftKey) {
        finishSelected.y = event.clientY;
        finishSelected.select = event.target;
    }
}

function clearNodes() {
    let children = document.getElementById("idOl").children;

    for (let child of children)
        child.className = "Normal";
}