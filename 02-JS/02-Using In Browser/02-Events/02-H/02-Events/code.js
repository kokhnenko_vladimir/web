document.getElementById('idMainDiv').onclick = function (event) {
    if (event.target.nodeName == "DIV") {
        let neighbors = event.target.parentNode.children;
        let curElement = event.target.children[0];
        
        for (let neighbor of neighbors) {
            for (let child of neighbor.children) {
                if (!child.hidden && child != curElement)
                    child.hidden = !child.hidden;
            }
        }

        curElement.hidden = !curElement.hidden;
    }
}