let input = document.getElementById("idInput");

document.getElementById("idInput").oninput = function (event) {
    event.preventDefault();
    let i = input.value.length - 1;
    if (!(input.value[i] >= "A" && input.value[i] <= "Z" || input.value[i] >= "a" && input.value[i] <= "z"))
        input.value = input.value.slice(0, i);
}