<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search files</title>
    <style>
    .flex-container {
                display: flex; /* блочный элемент */
                display: inline-flex; /* строчный элемент */
                flex-direction: column;
            }   
    .flex-item {
                text-align:center;
                font-size: 1em;
                padding: 1em;
                color: white;
            }
    .color1 {background-color: #675BA7;}
    .color2 {
            background-color: gray;
            text-align:left;
        }   
    
    </style>
</head>
<body>
    <div class="flex-container">
        
        <div class="flex-item color1">
            <form action="index.php" method="GET">
                <table>
                    <tbody>
                        <tr>
                            <td> <label for="idPath">Путь</label></td>
                            <td><input type="text" value="F:/test/simples/" name="pathFile"></td>
                        </tr>
                        <tr>
                            <td><label for="idMaskFile">Маска</label></td>
                            <td><input type="text" name="maskFile" id="idMaskFile" value="/.*.txt/u"></td>
                        </tr>
                        <tr>
                            <td><label for="idWord">Слово</label></td>
                            <td><input type="text" id="idWord" name="word"></td>
                        </tr>
                        <tr>
                        <td colspan="2"><input type="submit"></td>
                        </tr>
                    </tbody>
                </table>            
            </form>
        </div>
        <div class="flex-item color2">
        
        <?php

            $mainPathForDir = '';

            working();

            function working() {
                $resReq = isRequest();                

                if($resReq == 2) {
                    printFiles(searchFiles('names'));
                } else if($resReq == 1) {
                    searchWords(searchFiles('pathways'));
                }
            }

            function printFiles($files) {
                global $mainPathForDir;
                echo "Путь к папке - $mainPathForDir<br><br><br>";

                foreach($files as $file) {
                    echo "$file<br>";
                }
            }

            function lineOutput($fileName, $report) {
                echo "<p>$fileName</p>";
                echo "<table>";
                foreach($report as $line => $curLine) {
                    print "<tr><td>Строка №$line</td>";
                    foreach($curLine as $pos) {
                        print "<td>$pos</td>";
                    }
                    print "</tr>";
                }
                echo "</table>";
            }

            function searchWords($filesArr) {
                $search = $_GET['word'];
                $lenSearch = strlen($search);
                $pos = 0;

                $lengthArrFiles = count($filesArr);

                for($i = 0; $i < $lengthArrFiles; $i++) {
                    $filePath = $filesArr[$i];
                    $fd = fopen($filePath, "r") or die ("Cannot open file!");
                    $countLines = 0;
                    $report = [];
                    
                    while(!feof($fd)) {
                        
                        // Работа в файле
                        
                        $input = fgets($fd);
                        $positions = [];
                        $lineName = '';

                        $pos = mb_stripos($input, $search, $pos);
                        
                        if($pos !== false) {
                            $lineName = 1 + $countLines;
                            $fileName  = basename($filePath);
                        }

                        
                        while($pos !== false) {
                            $positions[] = $pos + 1;
                            $pos = mb_stripos($input, $search, $pos + $lenSearch);
                            
                        }

                        if(count($positions) > 0) {
                            $report[$lineName] = $positions;
                        }
                        ++$countLines;
                    }
                    if(count($report) > 0) {
                        lineOutput($fileName, $report);
                    }
                }
            }

            function searchFiles($type = 'names') {
                $dir = $_GET['pathFile'];

                if ($dir[strlen($dir) - 1] != '/') {
                    $dir .= '/';
                }
                
                global $mainPathForDir;
                $mainPathForDir = $dir;
                $pattern = $_GET['maskFile'];
                $result = [];

                if ($dh = opendir($dir)) {
    	            while (($file = readdir($dh)) !== false) {
                        if (is_file($dir.$file) && preg_match($pattern, $file)) {
                            if($type === 'names') {
                                $result[] = $file;
                            } else if($type === 'pathways') {
                                $result[] = $dir.$file;
                            }
                        }
	                }
	                // Закрываем процесс поиска
    	            closedir($dh);
	            }
                return $result;
            }

            function isRequest() {
                if(isset($_GET['pathFile']) && isset($_GET['maskFile']) && isset($_GET['word']) &&
                strlen($_GET['pathFile']) && strlen($_GET['maskFile']) && strlen($_GET['word'])) {
                    return 1;
                } else if(isset($_GET['pathFile']) && isset($_GET['maskFile']) && isset($_GET['word']) &&
                strlen($_GET['pathFile']) && strlen($_GET['maskFile'])) {
                    return 2;
                } else {
                    return 0;
                }
            }
        ?>
        
        </div>
    </div>
</body>
</html>