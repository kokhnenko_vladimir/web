<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add users</title>
</head>

<body>
    <div>
        <?php
            if(isset($_POST['login']) && mb_strlen($_POST['login']) && mb_strlen($_POST['password']) && mb_strlen($_POST['email'])) {
                $fd = fopen("users.txt", 'r') or die("не удалось открыть файл");
                $strings = array();
                $count = 0;

                while(!feof($fd))
                {
                    $str = htmlentities(fgets($fd));
                    $pos = mb_strpos($str, ';');
                    if ($pos) {
                        $resultStr = mb_substr($str, 0, $pos);
                        $strings[$count++] = $resultStr;
                    }                    
                }
                fclose($fd);
                
                $login = $_POST['login'];
                $userExists = true;
                foreach($strings as $item) {
                    if($item == $login) {
                        echo 'Такой пользователь уже есть';
                        $userExists = false;
                        break;
                    }
                }
                
                if($userExists) {
                    $password = $_POST['password'];
                    $email = $_POST['email'];

                    $userDate = array("$login", "$password", "$email");
                    writeFile($userDate);
                }
            } else {
                echo 'Все поля должны быть заполнены';
            }
        ?>

        <form action="addUser.php" method="post">
            <table>
                <tr>
                    <td><label for="idLogin">Login</label></td>
                    <td><input id="idLogin" type="text" name="login"></td>
                </tr>
                <tr>
                    <td><label for="idPassword">Password</label></td>
                    <td><input id="idPassword" type="password" name="password"></td>
                </tr>
                <tr>
                    <td><label for="idEmail">Email</label></td>
                    <td><input id="idEmail" type="text" name="email"></td>
                </tr>
                <tr>
                    <td><button type="submit">Save</button></td>
                </tr>
            </table>
        </form>
    </div>

    <?php
            function writeFile($userDate) {
                $fd = fopen("users.txt", 'a') or die("не удалось открыть файл");
                fwrite($fd, "$userDate[0];$userDate[1];$userDate[2];\n");
                fclose($fd);
            }
    ?>
</body>
</html>