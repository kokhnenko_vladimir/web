<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Select countries</title>
    <style>
        .wCl {
            width: 100%;
        }
    </style>
</head>
<body>
    <div>
        <form action="index.php" method="POST">
            <table>
                <tbody>
                    <tr>
                        <td><label for="idCountry">Страна</label></td>
                        <td><input type="text" name="country" id="idCountry"></td>
                    </tr>
                    <tr>
                    <td colspan="2"><input type="submit" class="wCl"></td>
                    </tr>
                </tbody>
            </table>
        </form>
        <?php


            working();

            function working() {
                if(!isset($_POST['country']) || !strlen($_POST['country'])) {
                    return false;
                }

                writeInFile($_POST['country'], isCountryAlready($_POST['country']));
                addSelect();
            }

            function addSelect() {
                if(!file_exists("countries.txt")) {
                    return false;
                }
                
                echo '<select name=\"select\" id=\"idSelect\">';
                
                $fd = fopen("countries.txt", 'r');
                while(!feof($fd)) {
                    $str = htmlentities(fgets($fd));
                    if($str != '') {
                        echo "<option value=\"$str\">$str</option>";
                    }
                }
                fclose($fd);

                echo "</select>";
            }

            function writeInFile($country, $contryIsAlready) {
                if(isCorrectCountry($country) && !$contryIsAlready) {
                    $fd = fopen("countries.txt", 'a') or die("не удалось открыть файл");
                    fwrite($fd, "$country\n");
                    fclose($fd);
                }
            }

            function isCorrectCountry($country) {
                define("A", 65);
                define("Z", 90);
                define("a", 97);
                define("z", 122);
                define("А", 1040);
                define("Я", 1071);
                define("а", 1072);
                define("я", 1103);
       
                if(uni_ord($country) >= A && uni_ord($country) <= Z || uni_ord($country) >= a && uni_ord($country) <= z) {
                    $fd = fopen("dictionary_en.txt", 'r') or die("не удалось открыть файл");
                } else if(uni_ord($country) >= А && uni_ord($country) <= Я || uni_ord($country) >= а && uni_ord($country) <= я) {
                    $fd = fopen("dictionary_ru.txt", 'r') or die("не удалось открыть файл");
                } else {
                    return false;
                }

                while(!feof($fd)){
                    $str = htmlentities(fgets($fd));
                    if($country == trim($str)) {
                        return true;
                    }                    
                }
                fclose($fd);

                return false;
            }
            
            function isCountryAlready($country) {
                if(file_exists("countries.txt")) {
                    $fd = fopen("countries.txt", 'r') or die("не удалось открыть файл");
                    while(!feof($fd)){
                        $str = htmlentities(fgets($fd));
                        if($country == trim($str)) {
                            fclose($fd);
                            return true;
                        }                    
                    }
                    fclose($fd);
                }
                return false;
            }

            function uni_ord($u) {
                $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
                $k1 = ord(substr($k, 0, 1));
                $k2 = ord(substr($k, 1, 1));
                return $k2 * 256 + $k1;
            }
        ?>
    </div>
</body>
</html>