<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Массивы</title>
	<style>
        body {
            font-family: Verdana, Arial, san-serif;
            font-size: 10pt;
        }
    </style>
</head>
<body>

<?php

    $units = array("ноль", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять");
    $dozens = array("десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто");
    $hundredsOf = array("сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот");
    $thousands = array("тысяча", "две тысячи", "три тысячи", "четыре тысячи", "пять тысячь", "шесть тысяч", "семь тысяч", "восемь тысяч", "девять тысяч");

    //
    //Не предусмотрены примеры такого плана 1005
    //

    $num = 2345;
    $count = 0;
    $stack = array();
    
    while($num > 0){
        $tmp = $num % 10;
        $stack[$count] = $tmp;
        $num =floor($num / 10);
        $count++;
    }

    $resultString;

    switch($count){
        case 1:
            $resultString = $units[$stack[0]];
        break;
        case 2:
            $resultString = $dozens[$stack[1] - 1] . ' ' . $units[$stack[0]];
        break;
        case 3:
            $resultString = $hundredsOf[$stack[2] - 1] . ' ' . $dozens[$stack[1] - 1] . ' ' . $units[$stack[0]];
        break;
        case 4:
            $resultString = $thousands[$stack[3] - 1] . ' ' . $hundredsOf[$stack[2] - 1] . ' ' . $dozens[$stack[1] - 1] . ' ' . $units[$stack[0]];
        break;
    }
    
    echo($resultString);
?>

</body>
</html>