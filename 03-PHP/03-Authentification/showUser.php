<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show users</title>
    <style>
        table tr, td {
            width: 10em;
        }

        tr.colorGray {
            background-color: gray;
        }
    </style>
</head>
<body>
    <div>
        <?php
            $fd = fopen("users.txt", 'r') or die("не удалось открыть файл");
            
            $count = 0;

            echo "<table><tr class=\"colorGray\"><td >Login</td><td>Password</td><td>Email</td></tr>";

            while(!feof($fd))
            {
                echo "<tr>";
                $strResult = '';

                $str = htmlentities(fgets($fd));
                $length = strlen($str);

                for($i = 0; $i < $length; $i++) {
                    if($str[$i] != ';') {
                        $strResult = $strResult . $str[$i];
                    } else {
                        echo "<td>$strResult</td>";
                        $strResult = '';
                    }
                }
                echo "</tr>";
            }
            fclose($fd);
            echo "</table>";
        ?>
    </div>
</body>
</html>