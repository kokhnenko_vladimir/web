<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autentification</title>
</head>
<body>
    <form action="login.php" method="POST">
        <div>
            <table>
                <tbody>
                    <tr>
                        <td>Login</td>
                        <td><input type="text" name="login"></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password"></td>
                    </tr>
                    <tr>
                        <td><button>Submit</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>

    <div>
        <?php 
            if(isCorrectUser() === 1) {
                echo 'Такой пользователь существует';
            } else if (isCorrectUser() === 2) {
                echo 'Нет такого пользователя';
            }
        
            function isCorrectUser() {
                if(!isset($_POST['login']) || !isset($_POST['password']) || !mb_strlen($_POST['login']) || !mb_strlen($_POST['password'])) {
                    return 0;
                }

                $fd = fopen("users.txt", 'r') or die("не удалось открыть файл");
                
                while(!feof($fd)){
                    $count = 0;
                    $str = htmlentities(fgets($fd));
                    
                    $userLogin = '';
                    $userPass = '';
                    $isExists = true;
                    $length = mb_strlen($str);

                    for($i = 0; $i < $length; $i++) {
                        if(!$count) {
                            if($str[$i] != ';') {
                                $userLogin = $userLogin . $str[$i];
                            } else {
                                $count++;
                            }
                        } else if($count == 1) {
                            if($str[$i] != ';') {
                                $userPass = $userPass . $str[$i];
                            } else {
                                if($userLogin == $_POST['login'] && password_verify($_POST['password'], $userPass)) {
                                    fclose($fd);
                                    return 1;
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                }
                fclose($fd);
                
                return 2;                
            }
        ?>
    </div>
</body>
</html>